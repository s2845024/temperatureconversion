package nl.utwente.di.tempConversion;

import java.text.DecimalFormat;

public class Calculator {
    public DecimalFormat df;

    public Calculator(){
        df = new DecimalFormat("0.00");
    }

    public double getFahrenheit(String celsius){
        double fahrenheit = 0.0;
        fahrenheit = Double.parseDouble(celsius) * 1.8 + 32.0;
        return Double.parseDouble(df.format(fahrenheit));
    }

}
