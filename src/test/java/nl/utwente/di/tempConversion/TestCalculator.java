package nl.utwente.di.tempConversion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCalculator {
    @Test
    public void testBook1() throws Exception {
        Calculator calculator = new Calculator();
        double price = calculator.getFahrenheit("1");
        Assertions.assertEquals(33.8, price,0.0, "1 celsius in fahrenheit");
    }
}
